const express = require('express')
const cors = require('cors')

class AppController {
    constructor(){
        this.express = express();
        this.middlewares();
        this.routes();
        
    }

    middlewares(){
        this.express.use(express.json());
        this.express.use(cors())
    }

    routes(){
        const rotasApi = require('./routes/route')
        this.express.use('/hotel/', rotasApi)
        
        }
        
}

module.exports = new AppController().express