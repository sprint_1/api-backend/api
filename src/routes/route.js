const router = require('express').Router()
const usuarioController = require('../controller/usuarioController')

router.post('/cadastroUser/', usuarioController.postUser)
router.put('/atualizarUser/', usuarioController.putUser)
router.delete('/deleteUser/:id', usuarioController.deleteUser)
router.post('/login/', usuarioController.logUser)
router.post('/signin', usuarioController.signInUser)


module.exports = router;